//======= STICK HEADER ========//
	$(window).on('scroll', function() {
        if ($(window).scrollTop() >= 75) {
            $("body").addClass("fixed-header");
        }
        else {
            return $("body").removeClass("fixed-header");
        }
    });


	//======= STICK box_whatsapp ========//
	$(".box_whatsapp").hide();
	$(window).on('scroll', function() {
        if ($(window).scrollTop() >= 200) {
            $(".box_whatsapp").show();
        }
        else {
            return $(".box_whatsapp").hide();
        }
    });