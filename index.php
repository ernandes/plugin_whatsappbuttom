<?php 

function whatsapp(){

    /*ESSE LINK É A API ORIGINAL PARA COLOCAR OS DADOS DO TELEFONE E A MENSAGEM QUE VAI APARECER*/
    // link original : https://api.whatsapp.com/send?phone=5521993942254&text=Olá,%20Ernandes!.%20Contato%20pelo%20site.
    
    /* ESTA LINK: http://bit.ly/2htP3GK É UM LINK GERADO EM UM INCURTADOR DE URL, SERVE PARA QUE A PESSOA NÃO VEJA O TELEFONE NA URL DO BROWSER */
	$html = '<div class="box_whatsapp hidden-sm hidden-xs">
						<a href="http://bit.ly/2E15S8r" target="_blank">
							<div class="text-title">Preciso falar urgente?</div>
							<div class="icone"><i class="fa fa-whatsapp" aria-hidden="true"></i></div>
						</a>
					 </div>';

	$html .= '<div class="box_whatsappResponsivo hidden-lg hidden-md">
						<a href="http://bit.ly/2htP3GK" target="_blank">
							<div class="icone"><i class="fa fa-whatsapp" aria-hidden="true"></i></div>
						</a>
					 </div>';				 


	return $html;				 
}


?>